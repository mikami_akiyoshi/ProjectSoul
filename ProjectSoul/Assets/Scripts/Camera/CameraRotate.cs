﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using player;

public class CameraRotate : MonoBehaviour
{
    //回転させるスピード
    public float rotateSpeed = 3.0f;

    private bool isRotate = false;
    private float angle = 90;

    public PlayerControl playerScript;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //回転させる角度
        //float angle = Input.GetAxis("Horizontal") * rotateSpeed;
     

        if (Input.GetKeyUp(KeyCode.Space))
        {
            isRotate = true;

            playerScript.PlayerRotate();
        }

        if (isRotate)
        {
            //カメラを回転させる
            transform.RotateAround(new Vector3(0, 0, 0), Vector3.up, angle);

            isRotate = false;
            //if (angle < 90)
            //{
            //    angle++;
            //}
            //else
            //{
            //    isRotate = false;
            //}
        }
    }
}