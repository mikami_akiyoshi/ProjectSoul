﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneControlBase : MonoBehaviour
{
    private static SceneControlBase mInstance;

    public static SceneControlBase Instance
    {
        get{
            if (null == mInstance)
            {
                mInstance = (SceneControlBase)FindObjectOfType(typeof(SceneControlBase));
                if (null == mInstance)
                {
                    Debug.Log(" DataManager Instance Error ");
                }
            }
            return mInstance;
        }
    }

    public void SceneLoading(string _sceneName)
    {
        // シーン読み込み
        // ????(_sceneName)
    }
}
