﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadeScene : MonoBehaviour
{
    void Update()
    {
        // タイトル画面にいる時
        if (SceneManager.GetActiveScene().name == "TitleScene")
        {
            if (this.gameObject.activeInHierarchy == true)
            {
                SceneManager.LoadScene("StageSelect");
            }
        }

        // リザルト画面にいる時
        if (SceneManager.GetActiveScene().name == "ResultScene")
        {
            if (this.gameObject.activeInHierarchy == true)
            {
                SceneManager.LoadScene("TitleScene");
            }
        }
    }
}
