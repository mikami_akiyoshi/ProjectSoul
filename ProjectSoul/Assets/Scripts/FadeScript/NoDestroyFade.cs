﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoDestroyFade : MonoBehaviour
{
    void Update()
    {
        DontDestroyOnLoad(this);
    }
}
