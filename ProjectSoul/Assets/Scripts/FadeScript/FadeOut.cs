﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class FadeOut : MonoBehaviour
{
    [SerializeField]
    private Material _transitionOut;

    // トランジションが完全に終わった時
    [SerializeField]
    private UnityEvent OnComplete;

    void Start()
    {
        StartCoroutine(BeginTransition());

    }

    IEnumerator BeginTransition()
    {
        yield return new WaitForEndOfFrame();

        yield return Animate(_transitionOut, 1);
        if (OnComplete != null)
        {
            OnComplete.Invoke();
        }
    }

    /// <summary>
    /// time秒かけてトランジションを行う
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    IEnumerator Animate(Material material, float time)
    {
        GetComponent<Image>().material = material;
        float current = 0;
        while (current < time)
        {
            material.SetFloat("_Alpha", current / time);
            yield return new WaitForEndOfFrame();
            current += Time.deltaTime;
        }
        material.SetFloat("_Alpha", 1);
    }

}
