﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour
{
    // フェードマテリアル
    [SerializeField]
    private Material _transitionIn;
    [SerializeField]
    private Material _transitionOut;

    // 切り替え時
    [SerializeField]
    private UnityEvent OnTransition;

    // トランジションが完全に終わった時
    [SerializeField]
    private UnityEvent OnComplete;

    void Start()
    {
        StartCoroutine(BeginTransition());
    }

    void Update()
    {
        
    }

    IEnumerator BeginTransition()
    {
        yield return Animate(_transitionIn, 1);
        if(OnTransition != null)
        {
            OnTransition.Invoke();
        }
        yield return new WaitForEndOfFrame();

        yield return Animate(_transitionOut, 1);
        if(OnComplete != null)
        {
            OnComplete.Invoke();
        }
    }

    /// <summary>
    /// time秒かけてトランジションを行う
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    IEnumerator Animate(Material material, float time)
    {
        GetComponent<Image>().material = material;
        float current = 0;
        while (current < time)
        {
            material.SetFloat("_Alpha", current / time);
            yield return new WaitForEndOfFrame();
            current += Time.deltaTime;
        }
        material.SetFloat("_Alpha", 1);
    }
}