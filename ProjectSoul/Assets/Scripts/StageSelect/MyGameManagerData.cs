﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SelectManager
{
    [CreateAssetMenu(fileName = "MyGameManagerData", menuName = "MyGameManagerData")]
    public class MyGameManagerData : ScriptableObject
    {
        // 次のシーン名
        [SerializeField]
        private string nextSceneName;

        // データ初期化
        private void OnEnable()
        {
            // タイトルシーンの時だけリセット
            if(SceneManager.GetActiveScene().name == "TitleScene")
            {
                nextSceneName = "";
            }
        }

        public void SetNextSceneName(string nextSceneName)
        {
            this.nextSceneName = nextSceneName;
        }

        public string GetNextSceneName()
        {
            return nextSceneName;
        }
    }
}