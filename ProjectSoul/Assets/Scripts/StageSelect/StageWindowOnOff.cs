﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageWindowOnOff : MonoBehaviour
{
    public Animator animator;


    private void OnTriggerEnter(Collider other)
    {
        // animator.SetBool("Open", !animator.GetBool("Open"));
        animator.SetBool("Open", true);
    }

    private void OnTriggerExit(Collider other)
    {
        animator.SetBool("Open", false);
    }
}
