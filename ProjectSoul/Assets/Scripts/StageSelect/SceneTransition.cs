﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

namespace SelectManager
{
    public class SceneTransition : MonoBehaviour
    {
        private MyGameManagerData myGameManagerData;

        private void Start()
        {
            myGameManagerData = FindObjectOfType<MyGameManager>().GetMyGameManagerData();
        }

        public void GoToOtherScene(string stage)
        {
            StartCoroutine("Otherscene",stage);
        }

        public IEnumerator Otherscene(string stage)
        {
            yield return new WaitForSeconds(1f);
            // 次のシーンデータをMyGameManagerに保存
            myGameManagerData.SetNextSceneName(stage);
            SceneManager.LoadScene(stage);
        }

    }
}