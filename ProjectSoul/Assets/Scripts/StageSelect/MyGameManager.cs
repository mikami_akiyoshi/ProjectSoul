﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SelectManager
{
    public class MyGameManager : MonoBehaviour
    {
        private static MyGameManager myGameManager;

        // ゲーム全体で管理するデータ
        [SerializeField]
        private MyGameManagerData myGameManagerData = null;

        private void Awake()
        {
            if(myGameManager == null)
            {
                myGameManager = this;
                DontDestroyOnLoad(this);
            }
            //else
            //{
            //    Destroy(gameObject);
            //}
        }

        // MyGameManagerData を返す
        public MyGameManagerData GetMyGameManagerData()
        {
            return myGameManagerData;
        }
    }
}