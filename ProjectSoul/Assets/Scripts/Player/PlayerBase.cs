﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CollisionCheck;

namespace player
{
    public class PlayerBase : MonoBehaviour
    {
        // りじっとぼでえ
        protected Rigidbody2D rb2d;

        protected int[] absorptionCheck = new int[6];

        //=========================================================================
        //!	@func	Start
        //
        //!	@brief	初期化処理
        //!	@note	
        //=========================================================================
        public virtual void Start() { }

        //=========================================================================
        //!	@func	Update
        //
        //!	@brief	更新処理
        //!	@note	
        //=========================================================================
        public virtual void Update() { }

        public void Command(int number)
        {
            absorptionCheck[number] = 1;

            for (int i = 0; i < 6; i++)
            {
                Debug.Log(absorptionCheck[i]);
            }
        }
        //=========================================================================
        //!	@func	OnCollisionEnter
        //
        //!	@brief	なんらかのオブジェクトと当たった時
        //!	@note	tagで判定
        //=========================================================================
        public virtual void OnCollisionEnter(Collision collision) { }

        //=========================================================================
        //!	@func	InputCommand
        //
        //!	@brief	プレイヤー入力
        //!	@note	
        //=========================================================================
        public GameObject obj;
        protected void InputCommand()
        {
            if (Input.GetKeyUp(KeyCode.Z))
            { 
                Instantiate(obj, new Vector3(0.0f, 0.5f, 0.0f), Quaternion.identity);
                //obj.transform.parent = this.transform;
                Debug.Log("aaaaa");
            }

        }
    }
}

