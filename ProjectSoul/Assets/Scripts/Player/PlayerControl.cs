﻿//*****************************************************************************
//!	@file	PlayerControl.cs
//
//!	@brief	プレイヤーの操作
//!	@note	
//*****************************************************************************

//-----------------------------------------------------------------------------
// namespace declaration.
//-----------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CollisionCheck;

namespace player
{
    //-----------------------------------------------------------------------------
    //!	@class	PlayerControl
    //!	@brief	プレイヤーの操作クラス
    //-----------------------------------------------------------------------------
    public class PlayerControl : PlayerBase
    {
        public override void Start()
        {
            //this.rb2d = GetComponent<Rigidbody2D>();
        }

        public override void Update()
        {
            InputCommand();
        }

        public override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);

            if(collision.gameObject.tag == "Block_Normal")
            {
                //this.transform.Translate(0.0f, 1.0f, 0.0f, Space.World);
            }
        }

        public void PlayerRotate()
        {
            Quaternion rotate = transform.rotation;
            rotate.y = +90;
        }
    }
}
