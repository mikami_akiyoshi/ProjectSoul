﻿//*****************************************************************************
//!	@file	PrintLog.cs
//
//!	@brief	画面にログを出力する
//!	@note	Debagu.Log()に入れたものが画面に出力される
//*****************************************************************************

//------------------------------------------------------------------------------
// namespace declaration.
//------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic; // Queueのために必要
using UnityEngine;
using System.Text; // StringBuilderのために必要

//-----------------------------------------------------------------------------
//!	@class	PrintLog
//
//!	@brief	画面にログを出力する、クラス
//-----------------------------------------------------------------------------
public class PrintLog: MonoBehaviour
{
    // ログを何個まで保持するか
    [SerializeField] int m_MaxLogCount = 20;
    
    // 表示領域
    [SerializeField] Rect m_Area = new Rect(220, 0, 400, 400);

    // ログの文字列を入れておくためのQueue
    Queue<string> m_LogMessages = new Queue<string>();

    // ログの文字列を結合するのに使う
    StringBuilder m_StringBuilder = new StringBuilder();

    //=========================================================================
    //!	@func	Start
    //
    //!	@brief	初期化処理
    //!	@note	logMessageReceivedに登録していく
    //=========================================================================
    void Start()
    {
        // Application.logMessageReceivedに関数を登録しておくと、
        // ログが出力される際に呼んでくれる
        Application.logMessageReceived += LogReceived;
    }

    //=========================================================================
    //!	@func	LogReceived
    //
    //!	@brief	ログが出力される際に呼んでもらう
    //!	@note	ログに追加、上限超えていたら削除
    //=========================================================================
    void LogReceived(string text, string stackTrace, LogType type)
    {
        // ログをQueueに追加
        m_LogMessages.Enqueue(text);

        // ログの個数が上限を超えていたら、最古のものを削除する
        while (m_LogMessages.Count > m_MaxLogCount)
        {
            m_LogMessages.Dequeue();
        }
    }

    void OnGUI()
    {
        // StringBuilderの内容をリセット
        m_StringBuilder.Length = 0;

        // ログの文字列を結合する（1個ごとに末尾に改行を追加）
        foreach (string s in m_LogMessages)
        {
            m_StringBuilder.Append(s).Append(System.Environment.NewLine);
        }

        // 画面に表示
        GUI.Label(m_Area, m_StringBuilder.ToString());
    }
}