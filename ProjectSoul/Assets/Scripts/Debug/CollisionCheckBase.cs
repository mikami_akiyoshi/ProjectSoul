﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using player;

namespace CollisionCheck
{
    public class CollisionCheckBase : MonoBehaviour
    {
        public int number;

        public GameObject playerObject;
        public PlayerBase playerScript;

        // Start is called before the first frame update
        public void Start()
        {
           // playerObject.GetComponent<PlayerBase>().Command(1);
            //playerObject = GameObject.Find("PF_Player");
            //playerScript = playerObject.GetComponent<PlayerBase>().Command(1) ;
        }

        // Update is called once per frame
        public void Update()
        {

        }

        void OnCollisionEnter(Collision collision)
        {
            switch (collision.collider.gameObject.tag)
            {
                case "surface_1":
                    playerScript.Command(0);
                    Destroy(this.gameObject);

                    break;
                case "surface_2":
                    playerScript.Command(1);

                    Destroy(this.gameObject);
                    break;
                case "surface_3":
                    playerScript.Command(2);

                    Destroy(this.gameObject);
                    break;
                case "surface_4":
                    playerScript.Command(3);

                    Destroy(this.gameObject);
                    break;
                case "surface_5":
                    playerScript.Command(4);

                    Destroy(this.gameObject);
                    break;
                case "surface_6":
                    playerScript.Command(5);

                    Destroy(this.gameObject);
                    break;

            }
            //Debug.Log(collision.collider.gameObject.tag);
            //Debug.Log(collision.collider.gameObject.name); // ぶつかった相手の名前を取得
        }


    }
}
